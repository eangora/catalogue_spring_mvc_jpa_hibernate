package org.sid;

import org.sid.dao.ProduitRepository;
import org.sid.entities.Produit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class CatalogueSpringMvcJpaHibernateMavenApplication {

	public static void main(String[] args) {
		//Pour pouvoir acceder aux différentes ressources instanciées par spring
		ApplicationContext ctx=SpringApplication.run(CatalogueSpringMvcJpaHibernateMavenApplication.class, args);
		
		ProduitRepository produitRepository = ctx.getBean(ProduitRepository.class);
		produitRepository.save(new Produit("LX 345656",678,99));
		produitRepository.save(new Produit("Ordi HP 34566",670,90));
		produitRepository.save(new Produit("imprimante Epson",608,89));
		produitRepository.save(new Produit("Photocopieuse Epson",1400,70));
		
		produitRepository.findAll().forEach(p->System.out.println(p.getDesignation()));
	}

}
